# README #

A very simple web service using Node.JS and Express.JS

* There is a randomized number generator that will be produced as a json file sent to the web service that will process to sort in ascending and descending order and respond with an ISO date and the sorted process data. 
* To start type "node app.js" and go to the url "http://localhost:567".
* Mocha tests have been included as well.
* To start the test type "npm test".