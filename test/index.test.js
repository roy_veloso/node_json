/*var request = require('supertest')
  , express = require('express');
 
var app = require('../app');
 
describe('Index Page', function() {
  it("renders successfully", function(done) {
    request(app).get('/').expect(200, done);    
  })
})*/

	
var should = require('should');
var app = require('../app');
var request = require('supertest');

describe('POST /sort', function() {

it('should respond with 200 and the date_sorted key', function(done) {
    request(app)
      .post('/sort')
      .send({"nums":["16","2","21","12","7","6","4","5","28","15","1","22","29","17","31","14","0","23","25","27","11","20","19","8","18","24","13","3","10","30","9","26"],"sort":"desc"})
      .expect(200)
      .expect('Content-Type', /json/)
      .end(function(err, res) {
        if (err) done(err);
        res.body.should.have.property('date_sorted');
        done();
      });
  });
});