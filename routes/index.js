var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Virtual Co-Worker Node Examination' });
});

router.post('/sort', function(req, res, next) {
	var isodate = new Date().toISOString();
  	console.log(req.body) // populated!
  	console.log(JSON.stringify(req.body))
  	var unordered = req.body.nums
  	//console.log(req.body.nums)
 	switch(req.body.sort){
  		case 'asc':
  			var asc_data = unordered.sort(function(a, b){return a-b});
  			var response_data = {sorted: asc_data, date_sorted: isodate}
  			console.log(response_data)
  			res.json(response_data);
  			break
  		case 'desc':
  			var desc_data = unordered.sort(function(a, b){return b-a});
  			var response_data = {sorted: desc_data, date_sorted: isodate}
  			console.log(response_data)
  			res.json(response_data);
  			break;
  		default:
  			console.log('There is an error regarding the process')
  	}


  	
});


module.exports = router;
